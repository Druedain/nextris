#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Size {
    width: usize,
    height: usize,
}

impl Size {
    pub fn of(width: usize, height: usize) -> Size {
        Size { width, height }
    }

    pub fn one() -> Size {
        Size { width: 1, height: 1 }
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }
}
