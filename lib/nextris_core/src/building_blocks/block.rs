use super::position::Position;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Status {
    Empty,
    Occupied,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Block {
    status: Status,
}

impl Block {
    pub fn empty() -> Block {
        Block { status: Status::Empty }
    }

    pub fn occupied() -> Block {
        Block { status: Status::Occupied }
    }

    pub fn of(status: Status) -> Block {
        Block { status }
    }

    pub fn make_empty(self) -> Block {
        Block::empty()
    }

    pub fn occupy(self) -> Block {
        Block::occupied()
    }

    pub fn status(&self) -> &Status {
        &self.status
    }
}
