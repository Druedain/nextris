use std::cell::Cell;
use crate::building_blocks::position::Position;
use crate::building_blocks::size::Size;
use crate::building_blocks::block::{Status, Block};
use crate::building_blocks::figure::Figure;
use crate::building_blocks::rows::Rows;


pub struct Board {
    blocks: Rows,
    figure: Option<Figure>,
}

impl Board {
    pub fn of(size: Size) -> Board {
        Board {
            blocks: Board::prepare_cells_of(&size),
            figure: Option::None,
        }
    }

    fn prepare_cells_of(size: &Size) -> Rows {
        Rows::of_size(size)
    }

    pub fn with_figure(self, figure: Figure) -> Board {
        let blocks = self.blocks;
        Board {
            blocks,
            figure: Some(figure),
        }
    }

    pub fn insert(self, figure: Figure) -> Board {
        Board {
            blocks: self.blocks,
            figure: Some(figure),
        }
    }

    fn size(&self) -> Size {
        let rows = self.blocks
            .content();

        let height = rows.capacity();
        let width = rows.first()
            .unwrap()
            .content()
            .capacity();

        Size::of(width, height)
    }

    pub fn figure(&self) -> &Figure {
        self.figure()
    }
}

fn insertion_start(board: &Board, figure: &Figure) -> usize {
    let board_width = board.size().width();
    let figure_width = figure.width();

    let board_middle = (board_width + 1) / 2; // ceiling
    let figure_middle = (figure_width + 1) / 2; // ceiling

    println!("{a} - {b} = {ab}", a = board_middle, b = figure_middle, ab = board_middle - figure_middle);

    board_middle - figure_middle
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn when_board_is_3x3_then_figure_1x1_start_at_1() {
        let figure = Figure::one();
        let board = Board::of(Size::of(3, 3));

        let insertion_start = insertion_start(&board, &figure);

        assert_eq!(insertion_start, 1)
    }

    #[test]
    fn when_board_is_4x4_then_figure_3x3_start_at_0() {
        let figure = Figure::occupied(Size::of(3, 3));
        let board = Board::of(Size::of(4, 4));

        let insertion_start = insertion_start(&board, &figure);

        assert_eq!(insertion_start, 0)
    }

    #[test]
    fn when_board_is_5x5_then_figure_3x3_start_at_1() {
        let figure = Figure::occupied(Size::of(3, 3));
        let board = Board::of(Size::of(5, 5));

        let insertion_start = insertion_start(&board, &figure);

        assert_eq!(insertion_start, 1)
    }
}
