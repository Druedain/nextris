#[derive(Debug, Copy, Clone)]
pub struct Position {
    x: usize,
    y: usize,
}

impl Position {
    pub fn of(x: usize, y: usize) -> Position {
        Position { x, y }
    }

    pub fn zero() -> Position {
        Position { x: 0, y: 0 }
    }
}
