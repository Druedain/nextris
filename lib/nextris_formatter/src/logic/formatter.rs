use std::fmt::{Formatter, Display};
use std::fmt;

use nextris_core::building_blocks::block::{Block, Status};
use nextris_core::building_blocks::figure::Figure;
use nextris_core::building_blocks::rows::Row;

fn format_figure(figure: &Figure) -> String {
    let formatted: Vec<String> = figure.blocks()
        .content()
        .iter()
        .map(|row| format_row(row))
        .collect();

    if formatted.len() < 2 {
        return formatted
            .into_iter()
            .next()
            .unwrap_or(String::new());
    }

    formatted
        .iter()
        .fold(
            String::new(),
            |prev, next| format!("{}\n{}", prev, next),
        )
        .trim().to_string()
}

fn format_row(row: &Row) -> String {
    row.content()
        .iter()
        .map(|block| Formattable::new(*block))
        .fold(
            String::new(),
            |prev, next| format!("{}{}", prev, next),
        )
}

struct Formattable {
    block: Block,
}

impl Formattable {
    fn new(block: Block) -> Formattable {
        Formattable { block }
    }
}

impl Display for Formattable {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.block.status() {
            Status::Empty => write!(f, "."),
            Status::Occupied => write!(f, "#"),
        }
    }
}

#[cfg(test)]
mod tests {
    use nextris_core::building_blocks::figure::Figure;
    use crate::logic::formatter::format_figure;
    use nextris_core::building_blocks::size::Size;
    use nextris_core::building_blocks::block::Status;
    use nextris_core::building_blocks::rows::Row;

    #[test]
    fn test_formatting_one() {
        let figure = Figure::one();
        let formatted = format_figure(&figure);

        assert_eq!(formatted, "#")
    }

    #[test]
    fn test_formatting_2x2() {
        let figure = Figure::occupied(Size::of(2, 2));
        let formatted = format_figure(&figure);
        let expected = "
##
##
        ".trim();

        assert_eq!(formatted, expected)
    }

    #[test]
    fn test_formatting_L() {
        let figure = Figure::from_separate_rows(vec![
            Row::from_statuses(vec![Status::Occupied, Status::Empty]),
            Row::from_statuses(vec![Status::Occupied, Status::Empty]),
            Row::from_statuses(vec![Status::Occupied, Status::Occupied]),
        ]);
        let formatted = format_figure(&figure);
        let expected = "
#.
#.
##
        ".trim();

        assert_eq!(formatted, expected)
    }

    #[test]
    fn test_formatting_S() {
        let figure = Figure::from_separate_rows(vec![
            Row::from_statuses(vec![Status::Occupied, Status::Empty]),
            Row::from_statuses(vec![Status::Occupied, Status::Occupied]),
            Row::from_statuses(vec![Status::Empty, Status::Occupied]),
        ]);
        let formatted = format_figure(&figure);
        let expected = "
#.
##
.#
        ".trim();

        assert_eq!(formatted, expected)
    }
}

